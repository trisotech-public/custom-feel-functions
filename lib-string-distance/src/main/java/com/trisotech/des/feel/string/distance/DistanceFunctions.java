package com.trisotech.des.feel.string.distance;

import java.math.BigDecimal;
import java.util.Locale;

import org.apache.commons.text.similarity.CosineDistance;
import org.apache.commons.text.similarity.FuzzyScore;
import org.apache.commons.text.similarity.HammingDistance;
import org.apache.commons.text.similarity.JaroWinklerDistance;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.commons.text.similarity.LongestCommonSubsequenceDistance;

import com.trisotech.des.feel.annotations.Description;
import com.trisotech.des.feel.annotations.Example;
import com.trisotech.des.feel.annotations.Name;
import com.trisotech.des.feel.annotations.Required;

public class DistanceFunctions {

    @Name("cosine distance")
    @Description("Measures the cosine distance between two character sequences. Character sequences are converted into vectors through a simple tokenizer that works with a regular expression to split words in a sentence.")
    @Example(expression = "cosine distance(\"BPMN\", \"CMMN\")", result = "1")
    @Example(expression = "cosine distance(\"hello\", \"hi\")", result = "1")
    @Example(expression = "cosine distance(\"hello\", \"hello\")", result = "0")
    public static BigDecimal cosineDistance(@Name("left")
    @Required
    String left, @Name("right")
    @Required
    String right) {
        return BigDecimal.valueOf(new CosineDistance().apply(left, right).doubleValue());
    }

    @Name("levenshtein distance")
    @Description("An algorithm for measuring the difference between two character sequences. This is the number of changes needed to change one sequence into another, where each change is a single character modification (deletion, insertion or substitution).")
    @Example(expression = "levenshtein distance(\"hippo\", \"elephant\")", result = "7")
    @Example(expression = "levenshtein distance(\"hello\", \"hello\")", result = "0")
    @Example(expression = "levenshtein distance(\"frog\", \"fog\")", result = "1")
    public static BigDecimal levenshteinDistance(@Name("left")
    @Required
    String left, @Name("right")
    @Required
    String right) {
        return BigDecimal.valueOf(new LevenshteinDistance().apply(left, right).intValue());
    }

    @Name("hamming distance")
    @Description("The hamming distance between two strings of equal length is the number of positions at which the corresponding symbols are different.")
    @Example(expression = "hamming distance(\"DMN\", \"DMN\")", result = "0")
    @Example(expression = "hamming distance(\"BPMN\", \"CMMN\")", result = "2")
    public static BigDecimal hammingDistance(@Name("left")
    @Required
    String left, @Name("right")
    @Required
    String right) {
        return BigDecimal.valueOf(new HammingDistance().apply(left, right).intValue());
    }

    @Name("longest common subsequence distance")
    @Description("An edit distance algorithm based on the length of the longest common subsequence between two strings.")
    @Example(expression = "longest common subsequence distance(\"DMN\", \"DMN\")", result = "0")
    @Example(expression = "longest common subsequence distance(\"BPMN\", \"CMMN\")", result = "4")
    public static BigDecimal longestCommonSubsequenceDistance(@Name("left")
    @Required
    String left, @Name("right")
    @Required
    String right) {
        return BigDecimal.valueOf(new LongestCommonSubsequenceDistance().apply(left, right).intValue());
    }

    @Name("jaro winkler distance")
    @Description("Measures the Jaro-Winkler distance of two character sequences. It is the complementary of Jaro-Winkler similarity.")
    @Example(expression = "jaro-winkler distance(\"foo\", \"foo\")", result = "0")
    @Example(expression = "jaro-winkler distance(\"frog\", \"fog\")", result = "0.075")
    public static BigDecimal jaroWinklerDistance(@Name("left")
    @Required
    String left, @Name("right")
    @Required
    String right) {
        return BigDecimal.valueOf(new JaroWinklerDistance().apply(left, right).doubleValue());
    }

    @Name("fuzzy score")
    @Description("A matching algorithm that is similar to the searching algorithms implemented in editors such as Sublime Text, TextMate, Atom and others. One point is given for every matched character. Subsequent matches yield two bonus points. A higher score indicates a higher similarity.")
    @Example(expression = "fuzzy score(\"foo\", \"foo\")", result = "7")
    @Example(expression = "fuzzy score(\"frog\", \"fog\")", result = "5")
    public static BigDecimal fuzzyScore(@Name("left")
    @Required
    String left, @Name("right")
    @Required
    String right) {
        return BigDecimal.valueOf(new FuzzyScore(Locale.getDefault()).fuzzyScore(left, right).intValue());
    }

}
