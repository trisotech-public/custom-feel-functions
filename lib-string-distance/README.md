# String Distance

This sample FEEL function library is intended as an example. It exposes some advanced string distance functionality from the apache
commons library for text.

This library offers an example of relying on a third party library (org.apache.commons/commons-text) and packaging it together with the
FEEL functions.

Alternatively, it would have been possible to simply add the apache commons library along side the FEEL function library in the engine and
it would also have worked properly.