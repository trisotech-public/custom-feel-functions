package com.trisotech.des.feel.mvn.documentation;

import java.io.File;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.trisotech.des.feel.annotations.Description;
import com.trisotech.des.feel.annotations.Example;
import com.trisotech.des.feel.annotations.Name;
import com.trisotech.des.feel.annotations.Required;
import com.trisotech.des.feel.mvn.GenerateDMNBKMs;

public class DocumentedFunction {
    /**
     * Full definition with 1 parameter
     */
    @Name("documented")
    @Description("Documented")
    @Example(expression = "documented(\"X\")", result = "\"X\"")
    @Example(expression = "documented()", result = "null")
    public static String echo(@Required
    @Description("v description")
    @Name("value")
    String v) {
        return v;
    }

    /**
     * Not defined method with 1 parameter
     */
    @Name("not documented")
    public static BigDecimal echo(BigDecimal x) {
        return x;
    }

    @Test
    public void testDocumentedFunction() throws MojoExecutionException, ParserConfigurationException, XPathExpressionException, TransformerException {

        GenerateDMNBKMs generator = new GenerateDMNBKMs("test", "My Test", "Test Description", getClass().getPackageName());

        Document dmnXML = generator.generateFunctionsDescriptor(getClass().getClassLoader());
        Element definitions = dmnXML.getDocumentElement();

        XPath xPath = XPathFactory.newInstance().newXPath();

        xPath.setNamespaceContext(new NamespaceContext() {

            @Override
            public Iterator<String> getPrefixes(String namespaceURI) {
                if (definitions.getNamespaceURI().equals(namespaceURI)) {
                    return List.of("dmn").iterator();
                }
                if (definitions.getAttribute("xmlns:triso").equals(namespaceURI)) {
                    return List.of("triso").iterator();
                }
                return null;
            }

            @Override
            public String getPrefix(String namespaceURI) {
                if (definitions.getNamespaceURI().equals(namespaceURI)) {
                    return "dmn";
                }
                if (definitions.getAttribute("xmlns:triso").equals(namespaceURI)) {
                    return "triso";
                }
                return null;
            }

            @Override
            public String getNamespaceURI(String prefix) {
                if ("dmn".equals(prefix)) {
                    return definitions.getNamespaceURI();
                }
                if ("triso".equals(prefix)) {
                    return definitions.getAttribute("xmlns:triso");
                }
                return null;
            }
        });

        Assertions.assertEquals("test", definitions.getAttribute("id"));
        Assertions.assertEquals("My Test", definitions.getAttribute("name"));

        Assertions.assertEquals("Test Description", xPath.compile("dmn:description/text()").evaluate(definitions, XPathConstants.STRING));

        // We have two BKMs
        Assertions.assertEquals(2, ((NodeList) xPath.compile("dmn:businessKnowledgeModel").evaluate(definitions, XPathConstants.NODESET)).getLength());

        // Documented function
        Assertions.assertEquals(1,
                ((NodeList) xPath.compile("dmn:businessKnowledgeModel[@name=\"documented\"]").evaluate(definitions, XPathConstants.NODESET)).getLength());

        Element documentedBKM = (Element) ((NodeList) xPath.compile("dmn:businessKnowledgeModel[@name=\"documented\"]").evaluate(definitions,
                XPathConstants.NODESET)).item(0);

        Assertions.assertEquals("Documented", xPath.compile("dmn:description/text()").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals(2,
                ((NodeList) xPath.compile("dmn:extensionElements/triso:examples/triso:example").evaluate(documentedBKM, XPathConstants.NODESET)).getLength());

        Assertions.assertEquals("documented(\"X\")",
                xPath.compile("dmn:extensionElements/triso:examples/triso:example[1]/@expression").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("\"X\"",
                xPath.compile("dmn:extensionElements/triso:examples/triso:example[1]/@result").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("documented()",
                xPath.compile("dmn:extensionElements/triso:examples/triso:example[2]/@expression").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("null",
                xPath.compile("dmn:extensionElements/triso:examples/triso:example[2]/@result").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("string", xPath.compile("dmn:variable/@typeRef").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals(1,
                ((NodeList) xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]").evaluate(documentedBKM, XPathConstants.NODESET)).getLength());

        Assertions.assertEquals(1,
                ((NodeList) xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter").evaluate(documentedBKM, XPathConstants.NODESET))
                        .getLength());

        Assertions.assertEquals("value",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/@name").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("string",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/@typeRef").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("true",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/@required").evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("v description", xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/dmn:description/text()")
                .evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("class", xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[1]/dmn:variable/@name")
                .evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("\"" + getClass().getName() + "\"",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[1]/dmn:literalExpression/dmn:text/text()")
                        .evaluate(documentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("method signature", xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[2]/dmn:variable/@name")
                .evaluate(documentedBKM, XPathConstants.STRING));
        Assertions.assertEquals("\"echo(java.lang.String)\"",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[2]/dmn:literalExpression/dmn:text/text()")
                        .evaluate(documentedBKM, XPathConstants.STRING));

        // Not Documented function
        Assertions.assertEquals(1,
                ((NodeList) xPath.compile("dmn:businessKnowledgeModel[@name=\"not documented\"]").evaluate(definitions, XPathConstants.NODESET)).getLength());

        Element notDocumentedBKM = (Element) ((NodeList) xPath.compile("dmn:businessKnowledgeModel[@name=\"not documented\"]").evaluate(definitions,
                XPathConstants.NODESET)).item(0);

        Assertions.assertEquals(0, ((NodeList) xPath.compile("dmn:description/text()").evaluate(notDocumentedBKM, XPathConstants.NODESET)).getLength());

        Assertions.assertEquals(0,
                ((NodeList) xPath.compile("dmn:extensionElements/triso:examples/triso:example").evaluate(notDocumentedBKM, XPathConstants.NODESET))
                        .getLength());

        Assertions.assertEquals("number", xPath.compile("dmn:variable/@typeRef").evaluate(notDocumentedBKM, XPathConstants.STRING));

        Assertions.assertEquals(1,
                ((NodeList) xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]").evaluate(notDocumentedBKM, XPathConstants.NODESET)).getLength());

        Assertions.assertEquals(1,
                ((NodeList) xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter").evaluate(notDocumentedBKM, XPathConstants.NODESET))
                        .getLength());

        Assertions.assertEquals("p1",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/@name").evaluate(notDocumentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("number",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/@typeRef").evaluate(notDocumentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/@required").evaluate(notDocumentedBKM, XPathConstants.STRING));

        Assertions.assertEquals(0, ((NodeList) xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:formalParameter/dmn:description/text()")
                .evaluate(notDocumentedBKM, XPathConstants.NODESET)).getLength());

        Assertions.assertEquals("class", xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[1]/dmn:variable/@name")
                .evaluate(notDocumentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("\"" + getClass().getName() + "\"",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[1]/dmn:literalExpression/dmn:text/text()")
                        .evaluate(notDocumentedBKM, XPathConstants.STRING));

        Assertions.assertEquals("method signature", xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[2]/dmn:variable/@name")
                .evaluate(notDocumentedBKM, XPathConstants.STRING));
        Assertions.assertEquals("\"echo(java.math.BigDecimal)\"",
                xPath.compile("dmn:encapsulatedLogic[@kind=\"Java\"]/dmn:context/dmn:contextEntry[2]/dmn:literalExpression/dmn:text/text()")
                        .evaluate(notDocumentedBKM, XPathConstants.STRING));

    }

    @Test
    public void testImageEncoding() throws MojoExecutionException {
        GenerateDMNBKMs generator = new GenerateDMNBKMs("test", "My Test", "Test Description", new File("src/test/resources/image.png"),
                getClass().getPackageName());
        Document dmnXML = generator.generateFunctionsDescriptor(getClass().getClassLoader());
        Element definitions = dmnXML.getDocumentElement();

        XPath xPath = XPathFactory.newInstance().newXPath();

        xPath.setNamespaceContext(new NamespaceContext() {

            @Override
            public Iterator<String> getPrefixes(String namespaceURI) {
                if (definitions.getNamespaceURI().equals(namespaceURI)) {
                    return List.of("dmn").iterator();
                }
                if (definitions.getAttribute("xmlns:triso").equals(namespaceURI)) {
                    return List.of("triso").iterator();
                }
                return null;
            }

            @Override
            public String getPrefix(String namespaceURI) {
                if (definitions.getNamespaceURI().equals(namespaceURI)) {
                    return "dmn";
                }
                if (definitions.getAttribute("xmlns:triso").equals(namespaceURI)) {
                    return "triso";
                }
                return null;
            }

            @Override
            public String getNamespaceURI(String prefix) {
                if ("dmn".equals(prefix)) {
                    return definitions.getNamespaceURI();
                }
                if ("triso".equals(prefix)) {
                    return definitions.getAttribute("xmlns:triso");
                }
                return null;
            }
        });
        Assertions.assertEquals("image/png", definitions.getAttribute("triso:logoType"));
        Assertions.assertTrue(definitions.getAttribute("triso:logoData").length() > 0);
    }

}
