package com.trisotech.des.feel.mvn.datatypes.valid;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.chrono.ChronoPeriod;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.trisotech.des.feel.annotations.Name;
import com.trisotech.des.feel.mvn.GenerateDMNBKMs;

public class DataTypeFunctions {

    @Name("string")
    public static String s() {
        return null;
    }

    @Name("number")
    public static BigDecimal n() {
        return null;
    }

    @Name("booleanType")
    public static boolean b1() {
        return false;
    }

    @Name("booleanObject")
    public static Boolean b2() {
        return false;
    }

    @Name("date")
    public static LocalDate date() {
        return null;
    }

    @Name("days and time duration")
    public static Duration duration1() {
        return null;
    }

    @Name("years and months duration")
    public static ChronoPeriod duration2() {
        return null;
    }

    @Name("Any")
    public static Object o() {
        return null;
    }

    @Name("context")
    public static Map<String, Object> ctx() {
        return null;
    }

    @Name("list of Any")
    public static List<?> listOfAny() {
        return null;
    }

    @Name("list of string")
    public static List<String> listOfString() {
        return null;
    }

    @Name("void")
    public static void nothing() {
    }

    @Test
    public void testValidDataTypes() throws MojoExecutionException, XPathExpressionException, TransformerException {
        GenerateDMNBKMs generator = new GenerateDMNBKMs("test", "My Test", "Test Description", getClass().getPackageName());

        Document dmnXML = generator.generateFunctionsDescriptor(getClass().getClassLoader());
        Element definitions = dmnXML.getDocumentElement();

        XPath xPath = XPathFactory.newInstance().newXPath();

        xPath.setNamespaceContext(new NamespaceContext() {

            @Override
            public Iterator<String> getPrefixes(String namespaceURI) {
                if (definitions.getNamespaceURI().equals(namespaceURI)) {
                    return List.of("dmn").iterator();
                }
                if (definitions.getAttribute("xmlns:triso").equals(namespaceURI)) {
                    return List.of("triso").iterator();
                }
                return null;
            }

            @Override
            public String getPrefix(String namespaceURI) {
                if (definitions.getNamespaceURI().equals(namespaceURI)) {
                    return "dmn";
                }
                if (definitions.getAttribute("xmlns:triso").equals(namespaceURI)) {
                    return "triso";
                }
                return null;
            }

            @Override
            public String getNamespaceURI(String prefix) {
                if ("dmn".equals(prefix)) {
                    return definitions.getNamespaceURI();
                }
                if ("triso".equals(prefix)) {
                    return definitions.getAttribute("xmlns:triso");
                }
                return null;
            }
        });

        Assertions.assertEquals(12, ((NodeList) xPath.compile("dmn:businessKnowledgeModel").evaluate(definitions, XPathConstants.NODESET)).getLength());
        Assertions.assertEquals("string",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"string\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("number",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"number\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("boolean",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"booleanType\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("boolean",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"booleanObject\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("date",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"date\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("Any",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"Any\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("years and months duration", xPath
                .compile("dmn:businessKnowledgeModel[@name=\"years and months duration\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("days and time duration", xPath.compile("dmn:businessKnowledgeModel[@name=\"days and time duration\"]/dmn:variable/@typeRef")
                .evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("context",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"context\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("list",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"list of Any\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("list<string>",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"list of string\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

        Assertions.assertEquals("Any",
                xPath.compile("dmn:businessKnowledgeModel[@name=\"void\"]/dmn:variable/@typeRef").evaluate(definitions, XPathConstants.STRING));

    }
}
