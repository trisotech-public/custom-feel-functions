package com.trisotech.des.feel.mvn.datatypes.invalid;

import java.util.Date;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.trisotech.des.feel.annotations.Name;
import com.trisotech.des.feel.mvn.GenerateDMNBKMs;

public class InvalidDataTypeFunction {

    @Name("invalid date return type")
    public static Date returnsADate() {
        return null;
    }

    @Test
    public void testValidDataTypes() throws MojoExecutionException, XPathExpressionException, TransformerException {
        GenerateDMNBKMs generator = new GenerateDMNBKMs("test", "My Test", "Test Description", getClass().getPackageName());
        try {
            generator.generateFunctionsDescriptor(getClass().getClassLoader());
            Assertions.fail("Should not compile");
        } catch (MojoExecutionException e) {
            // success
        }
    }
}
