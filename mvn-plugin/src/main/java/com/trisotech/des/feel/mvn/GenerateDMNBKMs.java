package com.trisotech.des.feel.mvn;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoPeriod;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.trisotech.des.feel.annotations.Description;
import com.trisotech.des.feel.annotations.Example;
import com.trisotech.des.feel.annotations.Name;
import com.trisotech.des.feel.annotations.Required;

@Mojo(name = "generate-functions-dmn", defaultPhase = LifecyclePhase.PREPARE_PACKAGE, requiresDependencyResolution = ResolutionScope.RUNTIME, requiresProject = true)
public class GenerateDMNBKMs extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    @Parameter(property = "id")
    private String id;

    @Parameter(property = "name")
    private String name;

    @Parameter(property = "description")
    private String description;

    @Parameter(property = "image")
    private File image;

    @Parameter(property = "packageName")
    private String packageName;

    private static Logger LOGGER = LoggerFactory.getLogger(GenerateDMNBKMs.class);

    private static String DMN_NS = "https://www.omg.org/spec/DMN/20230324/MODEL/"; // DMN 1.5

    private static String TRISO_NS = "http://www.trisotech.com/2015/triso/modeling"; // Trisotech Extensions Namespace

    public GenerateDMNBKMs() {
    }

    public GenerateDMNBKMs(String id, String name, String description, String packageName) {
        // Test case constructor
        this.id = id;
        this.name = name;
        this.description = description;
        this.packageName = packageName;
    }

    public GenerateDMNBKMs(String id, String name, String description, File image, String packageName) {
        // Test case constructor
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.packageName = packageName;
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        if (id == null) {
            LOGGER.warn("Missing mvn-custom-functions-builder configuration key id, using the POM artifactId: " + project.getArtifactId());
            id = project.getArtifactId();
        }

        if (name == null) {
            LOGGER.warn("Missing mvn-custom-functions-builder configuration key name, using the id by default: " + id);
            name = id;
        }

        try {
            String outputDirectory = project.getBuild().getOutputDirectory();

            ClassLoader outputDirectoryCL = URLClassLoader.newInstance(new URL[] { new File(outputDirectory).toURI().toURL() }, getClass().getClassLoader());

            Document dmnModel = generateFunctionsDescriptor(outputDirectoryCL);

            File metaInf = new File(outputDirectory, "META-INF");
            metaInf.mkdirs();
            File outputFile = new File(metaInf, "functions.dmn");

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(dmnModel);
            StreamResult result = new StreamResult(outputFile);
            transformer.transform(source, result);

        } catch (IOException | TransformerException e) {
            throw new MojoFailureException(e.getMessage(), e);
        }
    }

    public Document generateFunctionsDescriptor(ClassLoader outputDirectoryCL) throws MojoExecutionException {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new MojoExecutionException("Could not create XML Builder", e);
        }

        Document dmnModel = docBuilder.newDocument();
        Element definitions = dmnModel.createElementNS(DMN_NS, "definitions");
        definitions.setAttribute("xmlns", DMN_NS);
        definitions.setAttribute("xmlns:triso", TRISO_NS);
        definitions.setAttribute("namespace", "http://www.trisotech.com/library/" + id);
        definitions.setAttribute("exporter", "Java BKM Generator");
        definitions.setAttribute("id", sanitizeId(id));
        definitions.setAttribute("name", name);

        dmnModel.appendChild(definitions);

        if (description != null) {
            Element descriptionElement = dmnModel.createElementNS(DMN_NS, "description");
            descriptionElement.setTextContent(description);
            definitions.appendChild(descriptionElement);
        }

        if (image != null) {
            if (!image.exists()) {
                throw new MojoExecutionException("The image " + image.getAbsolutePath() + " could not be found");
            }
            try {
                byte[] imagedata = Files.readAllBytes(image.toPath());
                BufferedImage icon = ImageIO.read(new ByteArrayInputStream(imagedata));
                if (icon.getWidth() != 32 || icon.getHeight() != 32) {
                    throw new MojoExecutionException("The image is not 32x32 " + image.getAbsolutePath());
                }
                definitions.setAttribute("triso:logoData", Base64.getEncoder().encodeToString(imagedata));
                definitions.setAttribute("triso:logoType", "image/" + image.getName().substring(image.getName().lastIndexOf(".") + 1).toLowerCase().trim());
                definitions.setAttribute("triso:logoChoice", "Custom");

            } catch (IOException e) {
                throw new MojoExecutionException("Could not read image " + image.getAbsolutePath(), e);
            }
        }

        ClassLoader projectCL;
        try {
            List classpathElements = project.getCompileClasspathElements();
            classpathElements.add(project.getBuild().getOutputDirectory());
            classpathElements.add(project.getBuild().getTestOutputDirectory());
            URL urls[] = new URL[classpathElements.size()];
            for (int i = 0; i < classpathElements.size(); ++i) {
                urls[i] = new File((String) classpathElements.get(i)).toURL();
            }
            projectCL = new URLClassLoader(urls, this.getClass().getClassLoader());
        } catch (Exception e) {
            projectCL = this.getClass().getClassLoader();
        }

        ConfigurationBuilder config = new ConfigurationBuilder();
        config.setScanners(Scanners.MethodsAnnotated);
        config.addClassLoaders(projectCL);

        if (packageName != null && packageName.length() > 0) {
            config.setUrls(ClasspathHelper.forPackage(packageName, outputDirectoryCL));
            final String prefix = packageName.replaceAll("\\.", "/");
            config.setInputsFilter(i -> i.startsWith(prefix));
        } else {
            config.setUrls(ClasspathHelper.forClassLoader(outputDirectoryCL));
        }

        Reflections reflections = new Reflections(config);

        Set<Method> methods = reflections.getMethodsAnnotatedWith(Name.class);

        if (methods.size() == 0) {
            throw new MojoExecutionException("Could not find any static function annotated with the " + Name.class.getName() + " annotation.");
        }

        for (Method method : methods) {

            String identifier = "Method " + method.getName() + " of class " + method.getDeclaringClass().getName();

            if (!Modifier.isStatic(method.getModifiers())) {
                throw new MojoExecutionException(identifier + " is not declared as a static method.");
            }

            if (!Modifier.isPublic(method.getModifiers())) {
                throw new MojoExecutionException(identifier + " need to be declared as a public method.");
            }

            Element businessKnowledgeModel = dmnModel.createElementNS(DMN_NS, "businessKnowledgeModel");

            businessKnowledgeModel.setAttribute("id", sanitizeId(method.getDeclaredAnnotation(Name.class).value()));
            businessKnowledgeModel.setAttribute("name",
                    sanitizeName(method.getDeclaredAnnotation(Name.class).value(), identifier + " @Name contain invalid characters."));

            Description descriptionAnnotation = method.getAnnotation(Description.class);
            if (descriptionAnnotation != null && descriptionAnnotation.value() != null && descriptionAnnotation.value().length() > 0) {
                Element descriptionElement = dmnModel.createElementNS(DMN_NS, "description");
                descriptionElement.setTextContent(descriptionAnnotation.value());
                businessKnowledgeModel.appendChild(descriptionElement);
            }

            Example[] examplesAnnotations = method.getAnnotationsByType(Example.class);
            if (examplesAnnotations != null) {
                Element extensionElements = dmnModel.createElementNS(DMN_NS, "extensionElements");
                for (Example exampleAnnotation : examplesAnnotations) {
                    Element example = dmnModel.createElementNS(TRISO_NS, "example");
                    example.setAttribute("expression", exampleAnnotation.expression());
                    example.setAttribute("result", exampleAnnotation.result());
                    extensionElements.appendChild(example);
                }
                businessKnowledgeModel.appendChild(extensionElements);
            }

            Element variable = dmnModel.createElementNS(DMN_NS, "variable");
            variable.setAttribute("name", sanitizeName(method.getDeclaredAnnotation(Name.class).value(), identifier + " @Name contain invalid characters."));
            variable.setAttribute("typeRef", convertJavaTypeToFEELType(method.getGenericReturnType(),
                    "The return type of the method '" + identifier + "' can't be converted to a FEEL type: " + method.getReturnType()));
            businessKnowledgeModel.appendChild(variable);

            Element encapsulatedLogic = dmnModel.createElementNS(DMN_NS, "encapsulatedLogic");
            encapsulatedLogic.setAttribute("kind", "Java");
            encapsulatedLogic.setAttribute("typeRef", convertJavaTypeToFEELType(method.getGenericReturnType(),
                    "The return type of the method '" + identifier + "' can't be converted to a FEEL type: " + method.getReturnType()));
            businessKnowledgeModel.appendChild(encapsulatedLogic);

            if (method.getParameterCount() > 0) {
                java.lang.reflect.Parameter[] classParameters = method.getParameters();
                Type[] classParameterTypes = method.getGenericParameterTypes();
                for (int p = 0; p < classParameters.length; p++) {
                    Element formalParameter = dmnModel.createElementNS(DMN_NS, "formalParameter");
                    String name = null;
                    Name annotation = classParameters[p].getAnnotation(Name.class);
                    if (annotation != null) {
                        name = sanitizeName(annotation.value(), "Parameter " + annotation.value() + " of " + identifier + " contains invalid characters.");
                    } else {
                        name = "p" + (p + 1);
                    }
                    formalParameter.setAttribute("name", name);
                    formalParameter.setAttribute("typeRef", convertJavaTypeToFEELType(classParameterTypes[p], "The parameter type '" + name
                            + "' of the method '" + identifier + "' can't be converted to a FEEL type: " + classParameterTypes[p]));
                    if (classParameters[p].getAnnotation(Description.class) != null) {
                        Element descriptionElement = dmnModel.createElementNS(DMN_NS, "description");
                        descriptionElement.setTextContent(classParameters[p].getAnnotation(Description.class).value());
                        formalParameter.appendChild(descriptionElement);
                    }
                    if (classParameters[p].getAnnotation(Required.class) != null) {
                        formalParameter.setAttribute("triso:required", "true");
                    }
                    encapsulatedLogic.appendChild(formalParameter);
                }
            }

            Element context = dmnModel.createElementNS(DMN_NS, "context");

            Element classEntry = dmnModel.createElementNS(DMN_NS, "contextEntry");
            Element classVariable = dmnModel.createElementNS(DMN_NS, "variable");
            classVariable.setAttribute("name", "class");
            classVariable.setAttribute("typeRef", "string");
            classEntry.appendChild(classVariable);
            Element classExpression = dmnModel.createElementNS(DMN_NS, "literalExpression");
            Element classExpressionText = dmnModel.createElementNS(DMN_NS, "text");
            classExpressionText.setTextContent("\"" + method.getDeclaringClass().getName() + "\"");
            classExpression.appendChild(classExpressionText);
            classEntry.appendChild(classExpression);
            context.appendChild(classEntry);

            Element methodEntry = dmnModel.createElementNS(DMN_NS, "contextEntry");
            Element methodVariable = dmnModel.createElementNS(DMN_NS, "variable");
            methodVariable.setAttribute("name", "method signature");
            methodVariable.setAttribute("typeRef", "string");
            methodEntry.appendChild(methodVariable);
            Element methodExpression = dmnModel.createElementNS(DMN_NS, "literalExpression");
            Element methodExpressionText = dmnModel.createElementNS(DMN_NS, "text");
            String methodAsString = method.getName() + "(";
            java.lang.reflect.Parameter[] parameters = method.getParameters();
            if (parameters != null) {
                methodAsString += Arrays.asList(parameters).stream().map(p -> p.getType().getName()).collect(Collectors.joining(","));
            }
            methodAsString += ")";
            methodExpressionText.setTextContent("\"" + methodAsString + "\"");
            methodExpression.appendChild(methodExpressionText);
            methodEntry.appendChild(methodExpression);
            context.appendChild(methodEntry);

            encapsulatedLogic.appendChild(context);

            definitions.appendChild(businessKnowledgeModel);
        }
        return dmnModel;
    }

    private String convertJavaTypeToFEELType(Type type, String errorMessage) throws MojoExecutionException {

        Class<?> clazz = null;
        Class<?> genericType = null;

        if (type instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) type;
            clazz = (Class<?>) pType.getRawType();
            if (pType.getActualTypeArguments()[0] instanceof Class<?>) {
                genericType = (Class<?>) pType.getActualTypeArguments()[0];
            }
        }

        if (type instanceof Class) {
            clazz = (Class<?>) type;
        }

        if (clazz.equals(String.class)) {
            return "string";
        }
        if (clazz.equals(BigDecimal.class)) {
            return "number";
        }

        if (clazz.equals(Boolean.class) || clazz.equals(boolean.class)) {
            return "boolean";
        }
        if (clazz.equals(LocalDate.class)) {
            return "date";
        }

        if (clazz.equals(LocalDateTime.class) || clazz.equals(OffsetDateTime.class) || clazz.equals(ZonedDateTime.class)) {
            return "date and time";
        }

        if (clazz.equals(LocalTime.class) || clazz.equals(OffsetTime.class)) {
            return "time";
        }
        if (clazz.equals(Duration.class)) {
            return "days and time duration";
        }

        if (clazz.equals(ChronoPeriod.class)) {
            return "years and months duration";
        }
        if (clazz.equals(Object.class) || void.class.equals(clazz)) {
            return "Any";
        }

        if (Collection.class.isAssignableFrom(clazz)) {
            if (genericType != null) {
                return "list<" + convertJavaTypeToFEELType(genericType, errorMessage) + ">";
            } else {
                return "list";
            }
        }
        if (Map.class.isAssignableFrom(clazz)) {
            return "context";
        }

        throw new MojoExecutionException(errorMessage);
    }

    private String sanitizeName(String name, String errorMessage) throws MojoExecutionException {
        if (!name.matches("[a-zA-Z0-9 ]*")) {
            throw new MojoExecutionException(errorMessage);
        }
        return name;
    }

    private String sanitizeId(String id) throws MojoExecutionException {
        String newId = id.replaceAll("[^A-Za-z0-9]", "_");
        if (Character.isDigit(newId.charAt(0))) {
            return "_" + newId;
        } else {
            return newId;
        }
    }

}
