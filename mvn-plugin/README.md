# Trisotech Digital Enterprise Suite Custom FEEL Functions Maven Plugin

This maven plugins generates a `META-INF/functions.dmn` file inside of the target jar file of a library.

The Decision Model and Notation (DMN) file contains one Business Knowledge Model (BKM) per java static method annotated with the **@Name** annotation from the `Trisotech Digital Enterprise Suite Custom FEEL Functions Annotations` project.

The generated DMN file respect the DMN 1.5 specification for invoking Java methods from a DMN model (with some specificities around date/time/duration). 

The DMN model will work well in the `Trisotech Digital Automation Suite` but your millage may vary using a different engine. 

> **_NOTE:_**  If you have runtime dependencies on your library, you will need to either inlcude them in the jar using the `maven-shade-plugin` or make these dependencies available to the engine as well.

## Adding the maven plugin to one of your project

In your maven `pom.xml`, you will first need to include the annotations dependency in the provided scope:

```
	<dependencies>
		...
		<dependency>
			<groupId>com.trisotech.des.feel</groupId>
			<artifactId>des-annotations</artifactId>
			<version>1.0.0</version>
			<scope>provided</scope>
		</dependency>
		...		
	</dependencies>
```

Then add the build plugin that generates the DMN model at built time:

```
...
<build>
		<plugins>
			<plugin>
				<groupId>com.trisotech.des.feel</groupId>
				<artifactId>des-custom-functions-builder</artifactId>
				<version>1.0.0</version>
				<executions>
					<execution>
						<goals>
							<goal>generate-functions-dmn</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<id></id>
					<name></name>
					<description></description>
					<image></image>
					<packageName></packageName>
				</configuration>
			</plugin>
		</plugins>
	</build>
...

```	

The following configuration of the plugin are supported:

|Annotation   |Description                                             |Required  |Default      |
|-------------|--------------------------------------------------------|----------|-------------|
|id           |The identificator of the DMN model                      |false     | artifactId  |
|name         |The name of the DMN model                               |false     | artifactId  |
|description  |The documentation of the DMN model                      |false     | None        |
|image        |A 32x32 PNG image to represent this library             |false     | None        |
|packageName  |The package name (x.y.z format) to scan for annotations |false     | root package|


## Java vs DMN Data Type mapping

The annotated methods are required to be static but also are limited in the classes that can be used as return types and parameter types.

## Mapping between FEEL and Java

|FEEL type                  |Java Type                                                                   |
|---------------------------|----------------------------------------------------------------------------|
|number                     | java.math.BigDecimal                                                       |
|string                     | java.lang.String                                                           |
|date and time              | java.time.LocalDateTime, java.time.OffsetDateTime, java.time.ZonedDateTime |
|date                       | java.time.LocalDate                                                        |
|time                       | java.time.LocalTime, java.time.OffsetTime                                  |
|years and months duration  | java.time.chrono.ChronoPeriod                                              |
|days and time duration     | java.time.Duration                                                         |
|boolean                    | java.lang.Boolean                                                          |
|list                       | java.util.List<?>                                                          |
|context                    | java.util.Map<java.lang.String, ?>                                         |
|Any                        | java.lang.Object (Must be one of the types above)                          |

For lists and contexts, the java type marked with `?` needs to be one of the other valid Java types.



