# Trisotech Digital Enterprise Suite Custom FEEL Functions

This project contains the required libraries, utility and examples to get started in building your own FEEL native functions to use
in the Trisotech Digital Enterprise Suite.

- The `annotations` project offers annotations to document your FEEL functions.
- The `mvn-plugin` project offers a maven plugin to build your FEEL functions in your DevOps pipeline.

The mvn-plugin project `README.md` file provides the documentation on how to add the maven plugin to your build and what are the
mapping and annotations requirements for the Java functions.


## Setting Up the Project

In your maven `pom.xml`, you will first need to include the annotations dependency in the provided scope:

```
	<dependencies>
		...
		<dependency>
			<groupId>com.trisotech.des.feel</groupId>
			<artifactId>des-annotations</artifactId>
			<version>1.0.0</version>
			<scope>provided</scope>
		</dependency>
		...		
	</dependencies>
```

Then add the build plugin that generates the DMN model at built time:

```
...
<build>
	<plugins>
		<plugin>
			<groupId>com.trisotech.des.feel</groupId>
			<artifactId>des-custom-functions-builder</artifactId>
			<version>1.0.0</version>
			<executions>
				<execution>
					<goals>
						<goal>generate-functions-dmn</goal>
					</goals>
				</execution>
			</executions>
			<configuration>
				<id></id>
				<name></name>
				<description></description>
				<image></image>
				<packageName></packageName>
			</configuration>
		</plugin>
	</plugins>
</build>
...

```	

The following configuration of the plugin are supported:

|Annotation   |Description                                             |Required  |Default      |
|-------------|--------------------------------------------------------|----------|-------------|
|id           |The identificator of the DMN model                      |false     | artifactId  |
|name         |The name of the DMN model                               |false     | artifactId  |
|description  |The documentation of the DMN model                      |false     | None        |
|image        |A 32x32 PNG image to represent this library             |false     | None        |
|packageName  |The package name (x.y.z format) to scan for annotations |false     | root package|


### Runtime Dependencies

If you have runtime dependencies in your library, you will need to either inlcude them in the jar using the `maven-shade-plugin` or make these dependencies available to the engine as well.

To use the `maven-shade-plugin`, add the following to the `plugins` section of your `pom.xml` :
```
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.5.1</version>
        <configuration>
        </configuration>
        <executions>
            <execution>
                <phase>package</phase>
                <goals>
                    <goal>shade</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
```

The `lib-string-distance` project offers an example of a FEEL library exposing some algorithms to calculate the distance between two strings using an apache commons library.


## Creating the Custom Functions

The project can be developped as a normal Java project. To make a function available as a FEEL function, the function is required to be **marked static** and annotated with the `@Name` annotation.

The following annotation can be added on methods:

|Annotation     | Required   | Default   |
|---------------|------------|-----------|
|`@Name`        | true       | -         |
|`@Description` | false      | None      |
|`@Example`     | false      | None      |


The following annotation can be added on method parameters:

|Annotation     | Required   | Default   |
|---------------|------------|-----------|
|`@Name`        | true       | pX        |
|`@Description` | false      | None      |
|`@Required`    | false      | false     |


The functions available as FEEL functions and their parameters must be a type that corresponds with a FEEL type. The correspondance is as follows :

|FEEL type                  |Java Type                                                                   |
|---------------------------|----------------------------------------------------------------------------|
|number                     | java.math.BigDecimal                                                       |
|string                     | java.lang.String                                                           |
|date and time              | java.time.LocalDateTime, java.time.OffsetDateTime, java.time.ZonedDateTime |
|date                       | java.time.LocalDate                                                        |
|time                       | java.time.LocalTime, java.time.OffsetTime                                  |
|years and months duration  | java.time.chrono.ChronoPeriod                                              |
|days and time duration     | java.time.Duration                                                         |
|boolean                    | java.lang.Boolean                                                          |
|list                       | java.util.List<?>                                                          |
|context                    | java.util.Map<java.lang.String, ?>                                         |
|Any                        | java.lang.Object (Must be one of the types above)                          |

For lists and contexts, the java type marked with `?` needs to be one of the other valid Java types.


### Testing the Custom Functions

Unit tests can be created to test the custom functions. The `lib-jdk-math` project offers an example of some tests.


## Building the Project

To build the project, compile it with the following command :

`mvn clean install`

The build will be in the `target` folder of your project under te name `<project name>-<version>.jar`. This file can then be imported in the DES to have access to the custom functions.

## Examples

- The `lib-jdk-math` project offers an example of a FEEL library exposing some of the JDK math functionalities to FEEL.
- The `lib-string-distance` project offers an example of a FEEL library exposing some algorithms to calculate the distance between two strings using an apache commons library.

