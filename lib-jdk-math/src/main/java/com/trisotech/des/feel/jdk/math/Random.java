package com.trisotech.des.feel.jdk.math;

import java.math.BigDecimal;

import com.trisotech.des.feel.annotations.Description;
import com.trisotech.des.feel.annotations.Example;
import com.trisotech.des.feel.annotations.Name;

public class Random {

    @Name("random")
    @Description("Generate a random number from 0 to 1")
    @Example(expression = "random()", result = "0.3368472")
    public static BigDecimal random() {
        return BigDecimal.valueOf(Math.random());
    }

}
