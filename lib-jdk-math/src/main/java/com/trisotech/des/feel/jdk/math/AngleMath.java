package com.trisotech.des.feel.jdk.math;

import java.math.BigDecimal;

import com.trisotech.des.feel.annotations.Description;
import com.trisotech.des.feel.annotations.Example;
import com.trisotech.des.feel.annotations.Name;
import com.trisotech.des.feel.annotations.Required;

/**
 * 
 * Utility functions leveraged from the JDK. These expose Angle math logic.
 *
 */
public class AngleMath {

    @Name("to radians")
    @Description("Converts an angle measured in degrees to an approximately equivalent angle measured in radians.")
    @Example(expression = "to radians(90)", result = "1.5708")
    public static BigDecimal toRadians(@Name("angle")
    @Description("an angle, in degrees")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.toRadians(angle.doubleValue()));
    }

    @Name("to degrees")
    @Description("Converts an angle measured in degrees to an approximately equivalent angle measured in radians.")
    @Example(expression = "to degrees(1.5708)", result = "90")
    public static BigDecimal toDegrees(@Name("angle")
    @Description("an angle, in radians")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.toDegrees(angle.doubleValue()));
    }

    @Name("cos")
    @Description("Returns the trigonometric cosine of an angle.")
    @Example(expression = "cos(1.5708)", result = "0")
    @Example(expression = "cos(0.785398)", result = "0.707107")
    public static BigDecimal cos(@Name("angle")
    @Description("an angle, in radians")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.cos(angle.doubleValue()));
    }

    @Name("sin")
    @Description("Returns the trigonometric sine of an angle.")
    @Example(expression = "sin(1.5708)", result = "1")
    @Example(expression = "sin(0.785398)", result = "0.707107")
    public static BigDecimal sin(@Name("angle")
    @Description("an angle, in radians")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.sin(angle.doubleValue()));
    }

    @Name("tan")
    @Description("Returns the trigonometric tangent of an angle.")
    @Example(expression = "tan(1.5708)", result = "-272241.808409")
    @Example(expression = "tan(0.785398)", result = "1")
    public static BigDecimal tan(@Name("angle")
    @Description("an angle, in radians")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.tan(angle.doubleValue()));
    }

    @Name("arc cos")
    @Description("Returns the trigonometric arc cosine of an angle.")
    @Example(expression = "acos(0)", result = "1.5708")
    @Example(expression = "acos(0.707107)", result = "0.785398")
    public static BigDecimal acos(@Name("angle")
    @Description("an angle, in radians")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.acos(angle.doubleValue()));
    }

    @Name("arc sin")
    @Description("Returns the trigonometric arc sine of an angle.")
    @Example(expression = "asin(1)", result = "1.5708")
    @Example(expression = "asin(0.707107)", result = "0.785398")
    public static BigDecimal asin(@Name("angle")
    @Description("an angle, in radians")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.asin(angle.doubleValue()));
    }

    @Name("arc tan")
    @Description("Returns the trigonometric arc tangent of an angle.")
    @Example(expression = "tan(-272241.808409)", result = "-1.5708")
    @Example(expression = "tan(1)", result = "0.785398")
    public static BigDecimal atan(@Name("angle")
    @Description("an angle, in radians")
    @Required
    BigDecimal angle) {
        return BigDecimal.valueOf(Math.atan(angle.doubleValue()));
    }

}
