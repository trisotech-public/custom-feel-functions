package com.trisotech.des.feel.jdk.math;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LibraryTests {

    @Test
    public void testToDegrees() {
        Assertions.assertTrue(AngleMath.toDegrees(BigDecimal.valueOf(1.5708)).subtract(BigDecimal.valueOf(90)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testToRadians() {
        Assertions.assertTrue(AngleMath.toRadians(BigDecimal.valueOf(90)).subtract(BigDecimal.valueOf(1.5708)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testCos() {
        Assertions.assertTrue(AngleMath.cos(BigDecimal.valueOf(1.5708)).subtract(BigDecimal.valueOf(0)).abs().doubleValue() <= 0.001);
        Assertions.assertTrue(AngleMath.cos(BigDecimal.valueOf(0.785398)).subtract(BigDecimal.valueOf(0.707107)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testSin() {
        Assertions.assertTrue(AngleMath.sin(BigDecimal.valueOf(1.5708)).subtract(BigDecimal.valueOf(1)).abs().doubleValue() <= 0.001);
        Assertions.assertTrue(AngleMath.sin(BigDecimal.valueOf(0.785398)).subtract(BigDecimal.valueOf(0.707107)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testTan() {
        Assertions.assertTrue(AngleMath.tan(BigDecimal.valueOf(1.5708)).subtract(BigDecimal.valueOf(-272241.808409)).abs().doubleValue() <= 0.001);
        Assertions.assertTrue(AngleMath.tan(BigDecimal.valueOf(0.785398)).subtract(BigDecimal.valueOf(1)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testACos() {
        Assertions.assertTrue(AngleMath.acos(BigDecimal.valueOf(0)).subtract(BigDecimal.valueOf(1.5708)).abs().doubleValue() <= 0.001);
        Assertions.assertTrue(AngleMath.acos(BigDecimal.valueOf(0.707107)).subtract(BigDecimal.valueOf(0.785398)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testASin() {
        Assertions.assertTrue(AngleMath.asin(BigDecimal.valueOf(1)).subtract(BigDecimal.valueOf(1.5708)).abs().doubleValue() <= 0.001);
        Assertions.assertTrue(AngleMath.asin(BigDecimal.valueOf(0.707107)).subtract(BigDecimal.valueOf(0.785398)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testATan() {
        Assertions.assertTrue(AngleMath.atan(BigDecimal.valueOf(-272241.808409)).subtract(BigDecimal.valueOf(-1.5708)).abs().doubleValue() <= 0.001);
        Assertions.assertTrue(AngleMath.atan(BigDecimal.valueOf(1)).subtract(BigDecimal.valueOf(0.785398)).abs().doubleValue() <= 0.001);
    }

    @Test
    public void testRandom() {
        // Different numbers on every call between [0..1]
        Assertions.assertTrue(Random.random().doubleValue() <= 1);
        Assertions.assertTrue(Random.random().doubleValue() >= 0);
        Assertions.assertTrue(Random.random().subtract(Random.random()).abs().doubleValue() >= 0.0001);
    }
}
