# JDK Math

This sample FEEL function library is intended as an example. It exposes some JDK native functionality; in this case some math functionalities from
`java.lang.Math`.

This library also includes test cases for the functions written in JUnit 5.a

This library does not have any runtime dependencies other than the JDK and therefore will not package transitive dependencies.
