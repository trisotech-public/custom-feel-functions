# Trisotech Digital Enterprise Suite Custom FEEL Functions Annotations

This project contains annotations that can be used by the `Trisotech Digital Enterprise Suite Custom FEEL Functions Maven Plugin` to build a DMN model containing Buisness Knowledge Models (BKMs) invoking each of these functions.


To make a function available as a FEEL function, the function is required to be **marked static** and annotated with the `@Name` annotation.


The following annotation can be added on methods:

|Annotation     | Required   | Default   |
|---------------|------------|-----------|
|`@Name`        | true       | -         |
|`@Description` | false      | None      |
|`@Example`     | false      | None      |


The following annotation can be added on method parameters:

|Annotation     | Required   | Default   |
|---------------|------------|-----------|
|`@Name`        | true       | pX        |
|`@Description` | false      | None      |
|`@Required`    | false      | false     |



