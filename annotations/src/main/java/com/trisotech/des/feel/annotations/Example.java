package com.trisotech.des.feel.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Add an usage example to a method
 */

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(Examples.class)
@Target(ElementType.METHOD)
public @interface Example {
    public String expression();

    public String result();

}
